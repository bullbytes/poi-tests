# What is this?
Tests for reading and writing Microsoft Office files: https://poi.apache.org

# Build instructions

    ./gradlew run

# Prerequisites
JDK 11
